package clock.socoolby.com.clock.pop;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarLayout;
import com.haibin.calendarview.CalendarView;

import clock.socoolby.com.clock.R;
import razerdp.basepopup.BasePopupWindow;

public class CalendarPopup extends BasePopupWindow implements CalendarView.OnCalendarSelectListener, CalendarView.OnYearChangeListener, View.OnClickListener {
    public CalendarPopup(Context context) {
        super(context);
    }

    TextView mTextMonthDay;

    TextView mTextYear;

    TextView mTextLunar;

    TextView mTextCurrentDay;

    CalendarView mCalendarView;

    RelativeLayout mRelativeTool;
    private int mYear;
    CalendarLayout mCalendarLayout;


    @Override
    public View onCreateContentView() {
        View content=createPopupById(R.layout.pop_calendar);
        mTextMonthDay = (TextView) content.findViewById(R.id.tv_month_day);
        mTextYear = (TextView) content.findViewById(R.id.tv_year);
        mTextLunar = (TextView) content.findViewById(R.id.tv_lunar);
        mRelativeTool = (RelativeLayout) content.findViewById(R.id.rl_tool);
        mCalendarView = (CalendarView) content.findViewById(R.id.calendarView);
        mTextCurrentDay = (TextView) content.findViewById(R.id.tv_current_day);
        mTextMonthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCalendarLayout.isExpand()) {
                    mCalendarLayout.expand();
                    return;
                }
                mCalendarView.showYearSelectLayout(mYear);
                mTextLunar.setVisibility(View.GONE);
                mTextYear.setVisibility(View.GONE);
                mTextMonthDay.setText(String.valueOf(mYear));
            }
        });
        content.findViewById(R.id.fl_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.scrollToCurrent();
            }
        });
        content.findViewById(R.id.tv_calender_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mCalendarLayout = (CalendarLayout) content.findViewById(R.id.calendarLayout);
        mCalendarView.setOnCalendarSelectListener(this);
        mCalendarView.setOnYearChangeListener(this);
        mTextYear.setText(String.valueOf(mCalendarView.getCurYear()));
        mYear = mCalendarView.getCurYear();
        mTextMonthDay.setText(mCalendarView.getCurMonth() + "月" + mCalendarView.getCurDay() + "日");
        mTextLunar.setText("今日");
        mTextCurrentDay.setText(String.valueOf(mCalendarView.getCurDay()));
        mCalendarView.scrollToCurrent();
        return content;
    }

    public void setCurrentDay(){
        mCalendarView.scrollToCurrent();
    }


    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }


    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mTextLunar.setVisibility(View.VISIBLE);
        mTextYear.setVisibility(View.VISIBLE);
        mTextMonthDay.setText(calendar.getMonth() + "月" + calendar.getDay() + "日");
        mTextYear.setText(String.valueOf(calendar.getYear()));
        mTextLunar.setText(calendar.getLunar());
        mYear = calendar.getYear();
    }

    @Override
    public void onYearChange(int year) {
        mTextMonthDay.setText(String.valueOf(year));
    }

    @Override
    public void onClick(View view) {
    }

    public  String getCalendarFestival(){
        Calendar calendar=mCalendarView.getSelectedCalendar();
        String festival=calendar.getGregorianFestival();
        String ret=calendar.getTraditionFestival();
        if(ret==null&&festival==null)
            return null;
        if(ret==null||ret.isEmpty())
            return festival;
        if(festival==null||festival.isEmpty())
            return ret;
        return festival+"  "+ret;
    }
}
