package clock.socoolby.com.clock.widget.animatorview.animator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import java.util.HashMap;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

public  class DrawableArrayFallAnimator extends AbstractAnimator<DrawableArrayFallAnimator.Star> {

    /**
     * 小星星数目的最大值
     */
    public static final int MAX_NUM = 100;

    //小星星图片的资源文件
    int[] picRes ;

    public DrawableArrayFallAnimator(int[] picRes,int quantity){
        super(quantity);
        this.picRes=picRes;
    }

    /**
     * 小星星的数目
     */

    long prevTime,startTime;
    Matrix matrix = new Matrix();

    @Override
    public void start() {
        startTime = System.currentTimeMillis();
        prevTime = startTime;
        super.start();
    }

    @Override
    public Star createNewEntry() {
        randomColorIfAble();
        return createStar(width,picRes[rand.nextInt(picRes.length)],context,color);
    }


    @Override
    public void reset() {
        super.reset();
        clearCache();
    }


    @Override
    public boolean run() {
        //Log.d("123", "--------onAnimationUpdate--------");
        long nowTime = System.currentTimeMillis();
        float secs = (nowTime - prevTime) / 1000f;
        prevTime = nowTime;
        for (int i = 0; i < list.size(); i++) {
            Star star = list.get(i);
            star.y += (star.speed * secs);
            if (star.y > height) {
                star.y = 0 - star.height;
            }
            star.rotation = star.rotation + (star.rotationSpeed * secs);
        }
        return true;
    }


    @Override
    public void stop() {
        super.stop();
        clearCache();
    }

    /**
     * 添加小星星
     * @param quantity
     */
    public void addStars(int quantity) {
        if(list.size() <= MAX_NUM){
            for (int i = 0; i < quantity;i++) {
                randomColorIfAble();
                list.add(createNewEntry());
            }
        }
    }

    public  class Star implements I_AnimatorEntry {

        /**
         * x坐标、y坐标位置
         */
        float x, y;
        /**
         * 小星星旋转的角度
         */
        float rotation;
        /**
         * 小星星下落的速度
         */
        float speed;
        /**
         * 小星星旋转的速度
         */
        float rotationSpeed;
        /**
         * 小星星的宽和高
         */
        int width, height;

        /**
         * 对应的bitmap
         */
        Bitmap bitmap;

        @Override
        public void move(int maxWidth, int maxHight) {

        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            matrix.setTranslate(-width/2, -height/2);
            matrix.postRotate(rotation);
            matrix.postTranslate(width/2 + x, height/2 + y);
            canvas.drawBitmap(bitmap, matrix, null);
        }
    }

    static HashMap<Integer, Bitmap> bitmapCacheMap = new HashMap<Integer, Bitmap>();

    /**
     * @param context
     * @param xRange 小星星在X方向上的范围
     * @param resId 资源文件Id
     * @return
     */
     Star createStar(float xRange,int resId, Context context) {
        Star star = new Star();
        //如果十大图片则需要单独处理
        Bitmap originalBitmap = BitmapFactory.decodeResource(context.getResources(), resId);
        star.width = originalBitmap.getWidth();
        star.height = originalBitmap.getHeight();
        //初始坐标位置
        star.x = (float)Math.random() * (xRange - star.width);
        star.y = 0 - (star.height + (float)Math.random() * Util.getScreenHeight(context));
        //定义下落的速度
        star.speed = Util.dipToPX(context,50) + (float) Math.random() *  Util.dipToPX(context,50);
        //旋转角度及旋转速度(根据需要修改)
        star.rotation = (float) Math.random() * 180 - 90;
        star.rotationSpeed = (float) Math.random() * 90 - 45;
        //获得bitmap对象
        star.bitmap = bitmapCacheMap.get(resId);
        if (star.bitmap == null) {
            star.bitmap = Bitmap.createScaledBitmap(originalBitmap,
                    star.width, star.height, true);
            bitmapCacheMap.put(resId, star.bitmap);
        }
        return star;
    }

     Star createStar(float xRange,int resId, Context context,int color) {
        Star star = new Star();
        //如果十大图片则需要单独处理
        Drawable imageDraw=context.getResources().getDrawable(resId);
        imageDraw=Util.tint(imageDraw,color);
        Bitmap originalBitmap = Util.drawableToBitmap(imageDraw);
        star.width = originalBitmap.getWidth();
        star.height = originalBitmap.getHeight();
        //初始坐标位置
        star.x = (float)Math.random() * (xRange - star.width);
        star.y = 0 - (star.height + (float)Math.random() * Util.getScreenHeight(context));
        //定义下落的速度
        star.speed = Util.dipToPX(context,50) + (float) Math.random() *  Util.dipToPX(context,50);
        //旋转角度及旋转速度(根据需要修改)
        star.rotation = (float) Math.random() * 180 - 90;
        star.rotationSpeed = (float) Math.random() * 90 - 45;
        //获得bitmap对象
        star.bitmap = bitmapCacheMap.get(resId);
        if (star.bitmap == null) {
            star.bitmap = Bitmap.createScaledBitmap(originalBitmap,
                    star.width, star.height, true);
            bitmapCacheMap.put(resId, star.bitmap);
        }
        return star;
    }

    public static void clearCache(){
        bitmapCacheMap.clear();
    }
}
