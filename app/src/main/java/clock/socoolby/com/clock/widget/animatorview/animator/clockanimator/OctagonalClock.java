package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//八边形
public class OctagonalClock extends AbstractClock {

    /**
     * 根据控件的大小，初始化时钟刻度的长度和宽度、指针的长度和宽度、时钟中心点的半径
     */
    protected void initClockPointerLength() {
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        mDefaultScaleLength = mRadius / 10;
        mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        mParticularlyScaleLength = mRadius / 5;
        mParticularlyScaleWidth = mParticularlyScaleLength / 6;

        /*
         * 时针长=半径/3;
         * 时针宽=特殊时钟刻度宽;
         *
         * */
        mHourPointerLength = mRadius / 3;
        mHourPointerWidth = mParticularlyScaleWidth;

        /*
         * 分针长=半径/2;
         * 分针宽=特殊时钟刻度宽;
         *
         * */
        mMinutePointerLength = mRadius / 2;
        mMinutePointerWidth = mParticularlyScaleWidth;

        /*
         * 秒针长=半径/3*2;
         * 秒针宽=默认时钟刻度宽;
         *
         * */
        mSecondPointerLength = mRadius / 3 * 2;
        mSecondPointerWidth = mDefaultScaleWidth;

        // 中心点半径=（默认刻度宽+特殊刻度宽）/2
        mPointRadius = (mDefaultScaleWidth + mParticularlyScaleWidth) / 2;
    }

    /**
     * 绘制时钟的圆形和刻度
     * 分三段
     *    1. 上面右半从centerX,到centerX+octagonalLineLenght/2
     *    2. 1.1x+1.1y=mRadius+octagonalLineLenght/2
     *    3. 右面上半从centeY-octagonalLineLenght/2到centeY
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);
        float radianTanValue=0f;

        float stepLeng=0;
        float startX=0;
        float startY=-mRadius;
        float scaleLength=0;
        float floatLength=0;
        float octagonalLineLenght=new Double(Math.tan(Math.toRadians(45/2))*mRadius).floatValue()*2;
        float lineStartX=(0-octagonalLineLenght)/2;

        for(int i=0;i<4;i++){

            mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
            mDefaultPaint.setColor(mClockColor);

            canvas.drawLine(lineStartX,0,lineStartX+octagonalLineLenght/2,0,mDefaultPaint);

            canvas.drawLine(startX, 0, startX, 0 + mParticularlyScaleLength, mDefaultPaint);

            float temp=new Double(octagonalLineLenght/Math.sqrt(2)).floatValue();
            canvas.drawLine(lineStartX+octagonalLineLenght/2,0, lineStartX+octagonalLineLenght/2+temp,temp, mDefaultPaint);

            canvas.drawLine(lineStartX+octagonalLineLenght/2+temp,temp, lineStartX+octagonalLineLenght/2+temp,temp+octagonalLineLenght/2, mDefaultPaint);

            for(int k=1;k<4;k++){
                radianTanValue=new Double(Math.tan(Math.toRadians(6 *k))).floatValue();
                stepLeng=radianTanValue*hight/2;

                if (k % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength=mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength=mDefaultScaleLength;
                }
                floatLength=radianTanValue*scaleLength;
                canvas.drawLine(startX+stepLeng, startY, startX+stepLeng-floatLength, startY + scaleLength, mDefaultPaint);
                //canvas.drawLine(startX-stepLeng, 0, startX-stepLeng+floatLength, 0 + scaleLength, mDefaultPaint);

            }


            float radianPointLength=0;
            float scaleLengthCenterX=startX+new Double(mRadius/Math.sqrt(2)).floatValue();
            float scaleLengthCenterY=startY+new Double(mRadius/Math.sqrt(2)).floatValue();
            float radianPointX=0;
            float radianPointY=0;
            for(int k=4;k<8;k++){
                radianPointLength=new Double(Math.tan(Math.toRadians(45-k*6))*mRadius/Math.sqrt(2)).floatValue();
                radianPointX=scaleLengthCenterX-radianPointLength;
                radianPointY=scaleLengthCenterY-radianPointLength;
                radianTanValue=new Double(Math.cos(Math.toRadians(90-k*6))).floatValue();

                if (k % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength=mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength=mDefaultScaleLength;
                }
                floatLength=radianTanValue*scaleLength;

                canvas.drawLine(radianPointX, radianPointY, radianPointX-scaleLength, radianPointY+floatLength , mDefaultPaint);
            }

            for(int k=8;k<11;k++){
                radianPointLength=new Double(Math.tan(Math.toRadians(k*6-45))*mRadius/Math.sqrt(2)).floatValue();
                radianPointX=scaleLengthCenterX+radianPointLength;
                radianPointY=scaleLengthCenterY+radianPointLength;
                radianTanValue=new Double(Math.cos(Math.toRadians(90-k*6))).floatValue();

                if (k % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength=mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength=mDefaultScaleLength;
                }
                floatLength=radianTanValue*scaleLength;

                canvas.drawLine(radianPointX, radianPointY, radianPointX-scaleLength, radianPointY+floatLength , mDefaultPaint);
            }

            for(int k=11;k<15;k++){
                radianTanValue=new Double(Math.tan(Math.toRadians(90-k*6))).floatValue();
                stepLeng=radianTanValue*mRadius/2;
                if (k % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength=mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength=mDefaultScaleLength;
                }
                floatLength=radianTanValue*scaleLength;

                canvas.drawLine(startX+mRadius,startY-stepLeng, startX+mRadius - scaleLength,startY-stepLeng+floatLength, mDefaultPaint);
            }
            canvas.rotate(90);
        }

        canvas.restore();
    }


}
