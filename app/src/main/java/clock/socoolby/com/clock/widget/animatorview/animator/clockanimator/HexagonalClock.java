package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//六角形
public class HexagonalClock extends AbstractClock {

    /**
     * 根据控件的大小，初始化时钟刻度的长度和宽度、指针的长度和宽度、时钟中心点的半径
     */
    protected void initClockPointerLength() {
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        mDefaultScaleLength = mRadius / 10;
        mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        mParticularlyScaleLength = mRadius / 5;
        mParticularlyScaleWidth = mParticularlyScaleLength / 6;

        /*
         * 时针长=半径/3;
         * 时针宽=特殊时钟刻度宽;
         *
         * */
        mHourPointerLength = mRadius / 3;
        mHourPointerWidth = mParticularlyScaleWidth;

        /*
         * 分针长=半径/2;
         * 分针宽=特殊时钟刻度宽;
         *
         * */
        mMinutePointerLength = mRadius / 2;
        mMinutePointerWidth = mParticularlyScaleWidth;

        /*
         * 秒针长=半径/3*2;
         * 秒针宽=默认时钟刻度宽;
         *
         * */
        mSecondPointerLength = mRadius / 3 * 2;
        mSecondPointerWidth = mDefaultScaleWidth;

        // 中心点半径=（默认刻度宽+特殊刻度宽）/2
        mPointRadius = (mDefaultScaleWidth + mParticularlyScaleWidth) / 2;
    }
    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);

        float radianTanValue=0f;
        int j=1;
        float stepLeng=0;
        float startX=0;
        float startY=-mRadius;
        float scaleLength=0;
        float floatLength=0;
        float octagonalLineLenght=new Double(Math.tan(Math.toRadians(30))*mRadius).floatValue()*2;
        float lineStartX=(0-octagonalLineLenght)/2;

        for(int i=0;i<6;i++) {
            j=1;
            stepLeng=0;

            //边框，好像不画好看
            //mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
            //mDefaultPaint.setColor(mClockColor);
            //canvas.drawLine(lineStartX, startY, lineStartX+octagonalLineLenght,startY, mDefaultPaint);

            mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
            mDefaultPaint.setColor(mColorParticularyScale);
            canvas.drawLine(startX, startY, startX, startY + mParticularlyScaleLength, mDefaultPaint);
            while (stepLeng < octagonalLineLenght / 2) {//上下面
                radianTanValue = new Double(Math.tan(Math.toRadians(6 * j))).floatValue();
                stepLeng = radianTanValue * mRadius;

                if (j % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength = mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength = mDefaultScaleLength;
                }
                floatLength = radianTanValue * scaleLength;
                canvas.drawLine(startX + stepLeng, startY, startX + stepLeng - floatLength, startY + scaleLength, mDefaultPaint);
                canvas.drawLine(startX - stepLeng, startY, startX - stepLeng + floatLength, startY + scaleLength, mDefaultPaint);
                j++;
            }
            canvas.rotate(60);
        }
        canvas.restore();
    }


}
