package clock.socoolby.com.clock.widget.animatorview.animator.model.particle.drawer;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.I_PraticleDraw;
import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.Particle;

public class TriangleDraw implements I_PraticleDraw {
    public static  TriangleDraw getInstance(){
        return new TriangleDraw();
    }

    Direction direction;

    public TriangleDraw() {
        this(Direction.UP);
    }

    public TriangleDraw(Direction direction) {
        this.direction = direction;
    }

    @Override
    public void draw(Canvas canvas, Particle particle, Paint paint) {
        canvas.save();
        paint.setAlpha(particle.alpha);
        paint.setColor(particle.color);
        canvas.rotate(direction.degree,particle.x,particle.y);
        canvas.drawLine(particle.x, particle.y-particle.r, particle.x-particle.r,particle.y+particle.r, paint);
        canvas.drawLine(particle.x, particle.y-particle.r, particle.x+particle.r,particle.y+particle.r, paint);
        canvas.drawLine(particle.x-particle.r, particle.y+particle.r, particle.x+particle.r,particle.y+particle.r, paint);
        canvas.restore();
    }


    public enum Direction{
        LEFT(-270),RIGHT(-90),UP(0),DOWN(-180);
        public float degree;

        Direction(float degree) {
            this.degree = degree;
        }
    }
}