package clock.socoolby.com.clock.widget.animatorview.animator;


import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.AbstractClock;

// 参考自:https://github.com/ifadai/ClockDemo
public class ClockAnimator extends AbstractAnimator<AbstractClock> {
    private final String TAG = getClass().getSimpleName();

    AbstractClock clock;

    public ClockAnimator() {
        super(DYNAMIC_QUANTITY);
    }

    @Override
    public AbstractClock createNewEntry() {
        return null;
    }

    @Override
    public boolean run() {
        if(clock==null)
            return false;
        return super.run();
    }

    public void setClock(AbstractClock clock1){
        list.clear();
        this.clock=clock1;
        int textColor=color;
        randomColorIfAble();
        int minHigh=Math.min(width,height);
        clock.init(width,height,width/2,height/2,minHigh/2,color,textColor);
        list.add(clock);
    }
}
