package clock.socoolby.com.clock.model;

import android.graphics.Color;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.Constants;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.utils.FileUtils;

public class SharePerferenceModel implements Serializable {
    private int typeHourPower = Constants.TALKING_HALF_AN_HOUR;
    private final static String KEY_TYPE_HOUR_POWER = "key_type_hour_power";

    private DateModel startHourPowerTime = null;
    private final static String KEY_START_POWER = "key_start_power";
    private DateModel stopHourPowerTime = null;
    private final static String KEY_STOP_POWER = "key_stop_power";

    private boolean isDisplaySecond = true;
    private final static String KEY_IS_DISPLAY_SECOND = "key_is_display_second";
    private boolean isTickSound = false;
    private final static String KEY_IS_TICK_SOUND = "key_is_tick_sound";
    private boolean isTriggerScreen = true;
    private final static String KEY_IS_TRIGGER_SCREEN = "key_is_trigger_screen";

    private boolean hourSystem12 = false;
    private final static String KEY_IS_HOUR_SYSTEM_12 = "key_is_hour_system_12";


    private final static String KEY_CITY = "key_city";
    private String mCity;

    private final static String KEY_DESCRPTION = "key_description";
    private String mDescription;

    private final static String KEY_FONT_INDEX = "key_font_index";
    private Integer fontIndex;

    private final static String KEY_DISPLAYVIEW_TIME = "key_displayview_time";
    private final static String KEY_DISPLAYVIEW_DATE = "key_dsplayview_date";
    private final static String KEY_DISPLAYVIEW_DAY = "key_displayview_day";
    private final static String KEY_DISPLAYVIEW_WEATHER = "key_displayview_weather";
    private final static String KEY_DISPLAYVIEW_DESCRIPTION = "key_displayview_description";
    private JSONObject timeLocation = new JSONObject();
    private JSONObject dateLocation = new JSONObject();
    private JSONObject dayLocation = new JSONObject();
    private JSONObject weatherLocation = new JSONObject();
    private JSONObject descriptionLocation = new JSONObject();

    private final static String KEY_HANDUP_TIME="key_handup_time";
    private Integer handUpTime=-1;

    private final static String KEY_IS_HANDUP_ABLE="key_is_handup_able";
    private boolean handUpAble = false;

    private final static String KEY_BACKGROUND_COLOR="key_background_color";
    private Integer backgroundColor=Color.rgb(0, 0, 0);

    private final static String KEY_FOREGROUND_COLOR="key_foreground_color";
    private Integer foregroundColor=Color.rgb(255, 255, 255);

    private final static String KEY_FOREGROUND_COLOR1="key_foreground_color1";
    private Integer foregroundColor1=Color.rgb(199,21,133);

    public int getTypeHourPower() {
        return typeHourPower;
    }

    public void setTypeHourPower(int typeHourPower) {
        this.typeHourPower = typeHourPower;
    }

    public DateModel getStartHourPowerTime() {
        return startHourPowerTime;
    }


    public void setStartHourPowerTime(DateModel startHourPowerTime) {
        this.startHourPowerTime = startHourPowerTime;
    }

    public DateModel getStopHourPowerTime() {
        return stopHourPowerTime;
    }

    public void setStopHourPowerTime(DateModel stopHourPowerTime) {
        this.stopHourPowerTime = stopHourPowerTime;
    }

    public boolean isDisplaySecond() {
        return isDisplaySecond;
    }

    public void setDisplaySecond(boolean displaySecond) {
        isDisplaySecond = displaySecond;
    }

    public boolean isTickSound() {
        return isTickSound;
    }

    public void setTickSound(boolean tickSound) {
        isTickSound = tickSound;
    }

    public boolean isTriggerScreen() {
        return isTriggerScreen;
    }

    public void setTriggerScreen(boolean triggerScreen) {
        isTriggerScreen = triggerScreen;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        this.mCity = city;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setTimeLocation(JSONObject timeLocation) {
        this.timeLocation = timeLocation;
    }

    public void setDateLocation(JSONObject dateLocation) {
        this.dateLocation = dateLocation;
    }

    public void setDayLocation(JSONObject dayLocation) {
        this.dayLocation = dayLocation;
    }

    public void setWeatherLocation(JSONObject weatherLocation) {
        this.weatherLocation = weatherLocation;
    }

    public void setDescriptionLocation(JSONObject descriptionLocation) {
        this.descriptionLocation = descriptionLocation;
    }

    public JSONObject getTimeLocation() {
        return timeLocation;
    }

    public JSONObject getDateLocation() {
        return dateLocation;
    }

    public JSONObject getDayLocation() {
        return dayLocation;
    }

    public JSONObject getWeatherLocation() {
        return weatherLocation;
    }

    public JSONObject getDescriptionLocation() {
        return descriptionLocation;
    }

    private void fromJsonString(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            typeHourPower = jsonObject.getInt(KEY_TYPE_HOUR_POWER);
            isDisplaySecond = jsonObject.getBoolean(KEY_IS_DISPLAY_SECOND);
            isTickSound = jsonObject.getBoolean(KEY_IS_TICK_SOUND);
            isTriggerScreen =jsonObject.optBoolean(KEY_IS_TRIGGER_SCREEN,true);
            mCity = jsonObject.getString(KEY_CITY);
            mDescription = jsonObject.optString(KEY_DESCRPTION, ClockApplication.getContext().getResources().getString(R.string.always_zuo_never_die));
            startHourPowerTime = new DateModel();
            startHourPowerTime.setDataString(jsonObject.getString(KEY_START_POWER));
            stopHourPowerTime = new DateModel();
            stopHourPowerTime.setDataString(jsonObject.getString(KEY_STOP_POWER));
            timeLocation = new JSONObject(jsonObject.getString(KEY_DISPLAYVIEW_TIME));
            dateLocation = new JSONObject(jsonObject.getString(KEY_DISPLAYVIEW_DATE));
            dayLocation = new JSONObject(jsonObject.getString(KEY_DISPLAYVIEW_DAY));
            weatherLocation = new JSONObject(jsonObject.getString(KEY_DISPLAYVIEW_WEATHER));
            descriptionLocation = new JSONObject(jsonObject.getString(KEY_DISPLAYVIEW_DESCRIPTION));
            fontIndex = jsonObject.optInt(KEY_FONT_INDEX,0);
            handUpAble=jsonObject.optBoolean(KEY_IS_HANDUP_ABLE,false);
            handUpTime=jsonObject.optInt(KEY_HANDUP_TIME,-1);
            backgroundColor=jsonObject.optInt(KEY_BACKGROUND_COLOR,Color.rgb(0, 0, 0));
            foregroundColor=jsonObject.optInt(KEY_FOREGROUND_COLOR,Color.rgb(255, 255, 255));
            foregroundColor1=jsonObject.optInt(KEY_FOREGROUND_COLOR1,Color.rgb(199,21,133));
            hourSystem12=jsonObject.optBoolean(KEY_IS_HOUR_SYSTEM_12,false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String toJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_TYPE_HOUR_POWER, typeHourPower);
            jsonObject.put(KEY_IS_DISPLAY_SECOND, isDisplaySecond);
            jsonObject.put(KEY_IS_TICK_SOUND, isTickSound);
            jsonObject.put(KEY_IS_TRIGGER_SCREEN, isTriggerScreen);
            jsonObject.put(KEY_CITY, mCity);
            jsonObject.put(KEY_DESCRPTION, mDescription);
            jsonObject.put(KEY_START_POWER, startHourPowerTime.getTime());
            jsonObject.put(KEY_STOP_POWER, stopHourPowerTime.getTime());

            jsonObject.put(KEY_DISPLAYVIEW_TIME, timeLocation.toString());
            jsonObject.put(KEY_DISPLAYVIEW_DATE, dateLocation.toString());
            jsonObject.put(KEY_DISPLAYVIEW_DAY, dayLocation.toString());
            jsonObject.put(KEY_DISPLAYVIEW_WEATHER, weatherLocation.toString());
            jsonObject.put(KEY_DISPLAYVIEW_DESCRIPTION, descriptionLocation.toString());
            jsonObject.put(KEY_FONT_INDEX, fontIndex);
            jsonObject.put(KEY_HANDUP_TIME,handUpTime);
            jsonObject.put(KEY_IS_HANDUP_ABLE,handUpAble);
            jsonObject.put(KEY_BACKGROUND_COLOR,backgroundColor);
            jsonObject.put(KEY_FOREGROUND_COLOR,foregroundColor);
            jsonObject.put(KEY_FOREGROUND_COLOR1,foregroundColor1);
            jsonObject.put(KEY_IS_HOUR_SYSTEM_12,hourSystem12);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();

    }

    public void save() {
        Log.d("model","saveData :"+toString());
        FileUtils.writeObject(Constants.SHARE_PERFERENCE_FILE, toJsonString());
    }

    public void read() {
        fromJsonString((String) FileUtils.readObject(Constants.SHARE_PERFERENCE_FILE));
        Log.d("model","readData :"+toString());
    }


    @Override
    public String toString() {
        return "SharePerferenceModel{" +
                "typeHourPower=" + typeHourPower +
                ", startHourPowerTime=" + startHourPowerTime +
                ", stopHourPowerTime=" + stopHourPowerTime +
                ", isDisplaySecond=" + isDisplaySecond +
                ", isTickSound=" + isTickSound +
                ", mCity='" + mCity + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", timeLocation=" + timeLocation +
                ", dateLocation=" + dateLocation +
                ", dayLocation=" + dayLocation +
                ", weatherLocation=" + weatherLocation +
                ", descriptionLocation=" + descriptionLocation +
                ", fontIndex="+fontIndex+
                ", handUpAble="+handUpAble+
                ", handUpTime="+handUpTime+
                ", backgroundColor="+backgroundColor+
                ", foregroundColor="+foregroundColor+
                ", foregroundColor1="+foregroundColor1+
                ", hourSystem12="+hourSystem12+
                '}';
    }

    public void setFontIndex(int font_index) {
        fontIndex=font_index;
    }

    public Integer getFontIndex(){
        return fontIndex;
    }

    public Integer getHandUpTime() {
        return handUpTime;
    }

    public boolean isHandUpAble() {
        return handUpAble;
    }

    public void setHandUpTime(Integer handUpTime) {
        this.handUpTime = handUpTime;
    }

    public void setHandUpAble(boolean handUpAble) {
        this.handUpAble = handUpAble;
    }


    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Integer getForegroundColor() {
        return foregroundColor;
    }

    public void setForegroundColor(Integer foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    public Integer getForegroundColor1() {
        return foregroundColor1;
    }

    public void setForegroundColor1(Integer foregroundColor1) {
        this.foregroundColor1 = foregroundColor1;
    }

    public boolean isHourSystem12() {
        return hourSystem12;
    }

    public void setHourSystem12(boolean hourSystem12) {
        this.hourSystem12 = hourSystem12;
    }
}
