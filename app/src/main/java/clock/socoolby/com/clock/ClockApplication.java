package clock.socoolby.com.clock;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.Utils;

import clock.socoolby.com.clock.protocol.BusinessService;
import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.utils.FileUtils;
import clock.socoolby.com.clock.model.SharePerferenceModel;


public class ClockApplication extends Application {

    private static ClockApplication sEndzoneBoxApp;
    private BusinessService mBusinessService = new BusinessService();

    public static ClockApplication getInstance() {
        return sEndzoneBoxApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sEndzoneBoxApp = this;
        Utils.init(this);
        init();

    }


    public void init() {
        PermissionUtils.permission(PermissionConstants.SENSORS, PermissionConstants.STORAGE)
                .rationale(new PermissionUtils.OnRationaleListener(){
                    @Override
                    public void rationale(ShouldRequest shouldRequest) {
                        //DialogHelper.showRationaleDialog(shouldRequest);
                    }
                }).callback(new PermissionUtils.SimpleCallback() {
            @Override
            public void onGranted() {
                Log.d("app","supported permission....");
            }

            @Override
            public void onDenied() {
                Log.d("app","supported permission denied...");
            }
        }).request();

        if (!FileUtils.isExistsFile(Constants.SHARE_PERFERENCE_FILE)) {
            SharePerferenceModel model = new SharePerferenceModel();
            model.setTypeHourPower(Constants.TALKING_HOURS);
            DateModel startTimeModel = new DateModel();
            startTimeModel.setTime(12, 31);
            DateModel stopTimeModel = new DateModel();
            stopTimeModel.setTime(14, 31);
            model.setStartHourPowerTime(startTimeModel);
            model.setStopHourPowerTime(stopTimeModel);
            model.setCity(getString(R.string.shenzhen));
            model.save();
        }
    }

    public static Context getContext() {
        return ClockApplication.getInstance().getApplicationContext();
    }

    public BusinessService getBusinessService() {
        return mBusinessService;
    }

    private MainActivity mMainActivity;

    public void setMainActivity(MainActivity mainActivity) {
        this.mMainActivity = mainActivity;
    }

    public MainActivity getMainActivity() {
        return mMainActivity;
    }

}
