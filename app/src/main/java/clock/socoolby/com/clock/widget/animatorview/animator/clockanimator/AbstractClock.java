package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Calendar;

import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

public abstract class AbstractClock implements I_AnimatorEntry {
    protected int width,hight;

    protected int mCenterX, mCenterY;

    // 默认刻度画笔、指针画笔、文字画笔；
    protected Paint mDefaultPaint, mPointerPaint, mTextPaint;

    // 时钟半径、中心点半径、默认刻度长度、默认刻度宽度、特殊刻度长度、特殊刻度宽度、
    // 时指针长度、时钟指针宽度、分钟指针长度、分钟指针宽度、秒钟指针长度、秒钟指针宽度
    protected float mRadius, mPointRadius,
            mDefaultScaleLength, mDefaultScaleWidth,
            mParticularlyScaleLength, mParticularlyScaleWidth,
            mHourPointerLength, mHourPointerWidth,
            mMinutePointerLength, mMinutePointerWidth,
            mSecondPointerLength, mSecondPointerWidth;

    // 当前时、分、秒
    protected int mH, mM, mS;

    // 时钟颜色、默认刻度颜色、时刻度颜色、时针颜色、分针颜色、秒针颜色
    protected int mClockColor, mColorDefaultScale, mColorParticularyScale, mColorHourPointer,
            mColorMinutePointer, mColorSecondPointer,textColor;

    public AbstractClock() {
    }

    public void init(int  width,int hight,int mCenterX, int mCenterY, float mRadius, int mClockColor,int textColor){
        this.width=width;
        this.hight=hight;
        this.mCenterX = mCenterX;
        this.mCenterY = mCenterY;
        this.mRadius = mRadius;
        this.mClockColor = mClockColor;
        this.textColor=textColor;
        init();
    }

    private void init() {
        mColorDefaultScale =  mClockColor;
        mColorParticularyScale =  mClockColor;

        mColorHourPointer = mClockColor;
        mColorMinutePointer = mClockColor;
        mColorSecondPointer = mClockColor;

        mDefaultPaint = new Paint();
        mDefaultPaint.setAntiAlias(true);
        mDefaultPaint.setStyle(Paint.Style.STROKE);

        mPointerPaint = new Paint();
        mPointerPaint.setAntiAlias(true);
        mPointerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPointerPaint.setTextSize(14);
        mPointerPaint.setStrokeCap(Paint.Cap.ROUND);

        mTextPaint = new Paint();
        mPointerPaint.setAntiAlias(true);
        mPointerPaint.setStyle(Paint.Style.FILL);
        mPointerPaint.setColor(mClockColor);
        initClockPointerLength();
    }


    /**
     * 获取当前系统时间
     */
    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        hour = hour % 12;
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        if (hour != mH || minute != mM || second != mS) {
            setTime(hour, minute, second);
        }
    }

    /**
     * 设置时间
     */
    private void setTime(int h, int m, int s) {
        mH = h;
        mM = m;
        mS = s;
    }

    /**
     * 根据控件的大小，初始化时钟刻度的长度和宽度、指针的长度和宽度、时钟中心点的半径
     */
    protected abstract void initClockPointerLength();

    public void onDraw(Canvas canvas, Paint mPaint) {

        setTextPaint();
        drawBorder(canvas);
        drawOrnament(canvas);
        // 坐标原点移动到View 中心
        canvas.translate(mCenterX, mCenterY);
        drawText(canvas);
        drawPointer(canvas);

    }

    /**
     * 绘制 装饰
     * @param canvas
     */
    protected int loop=0;
    private char[] mark=new char[]{'w','s','l'};
    private int dealy=0;
    private char c;
    protected void drawOrnament(Canvas canvas){
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();

        // 文字顶部与基线距离
        float ascent = Math.abs(fontMetrics.ascent);
        // 文字底部与基线距离
        float descent = Math.abs(fontMetrics.descent);
        // 文字高度
        float fontHeight = ascent + descent;

        // 文字宽度
        float fontWidth=mTextPaint.measureText("w");

        // 开始XX
        float x = mCenterX-(fontWidth*mark.length)/2+fontWidth/2;

        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字顶部距离基线的距离）
        float y = mParticularlyScaleLength +fontHeight * 2;

        for(int i=0;i<mark.length;i++) {
            c=mark[i];
            if(i==loop)
                c=Character.toUpperCase(c);
            canvas.drawText(String.valueOf(c), x+fontWidth*i, y, mTextPaint);
        }
        if(dealy++>200) {
            dealy=0;
            loop++;
        }
        if(loop==mark.length)
            loop=0;
    }

    /**
     * 绘制时钟的圆形和刻度
     */
    protected abstract void drawBorder(Canvas canvas);


    /**
     * 绘制特殊时刻（12点、3点、6点、9点)的文字
     */
    protected  void drawText(Canvas canvas) {

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();

        // 文字顶部与基线距离
        float ascent = Math.abs(fontMetrics.ascent);
        // 文字底部与基线距离
        float descent = Math.abs(fontMetrics.descent);
        // 文字高度
        float fontHeight = ascent + descent;
        // 文字竖直中心点距离基线的距离；
        float offsetY = fontHeight / 2 - Math.abs(fontMetrics.descent);
        // 文字宽度
        float fontWidth;

        // drawText(@NonNull String text, float x, float y, @NonNull Paint paint) 参数：y，为基线的y坐标，并非文字左下角的坐标
        // 文字距离圆圈的距离为 特殊刻度长度+宽度

        String h = "12";
        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字顶部距离基线的距离）
        float y = -(mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - ascent);
        canvas.drawText(h, 0, y, mTextPaint);


        h = "3";
        fontWidth = mTextPaint.measureText(h);
        // y轴坐标为: 半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字长度/2（绘制原点在文字横向中心）
        y = mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - (fontWidth / 2);
        canvas.drawText(h, y, 0 + offsetY, mTextPaint);

        h = "6";
        // y轴坐标为: 半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字底部与基线的距离
        y = mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - descent;
        canvas.drawText(h, 0, y, mTextPaint);

        h = "9";
        fontWidth = mTextPaint.measureText(h);
        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字长度/2（绘制原点在文字横向中心））
        y = -(mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - (fontWidth / 2));
        canvas.drawText(h, y, 0 + offsetY, mTextPaint);
    }

    protected void setTextPaint() {
        mTextPaint.setStrokeWidth(mDefaultScaleWidth / 2);
        mTextPaint.setTextSize(mParticularlyScaleWidth * 4);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setColor(textColor);
    }

    /**
     * 绘制指针
     */
    protected void drawPointer(Canvas canvas) {
        drawHourPointer(canvas);
        drawMinutePointer(canvas);
        drawSecondPointer(canvas);

        mPointerPaint.setColor(mClockColor);
        // 绘制中心原点，需要在指针绘制完成后才能绘制
        canvas.drawCircle(0, 0, mPointRadius, mPointerPaint);
    }

    /**
     * 绘制时针
     */
    protected void drawHourPointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mHourPointerWidth);
        mPointerPaint.setColor(mColorHourPointer);

        // 当前时间的总秒数
        float s = mH * 60 * 60 + mM * 60 + mS;
        // 百分比
        float percentage = s / (12 * 60 * 60);
        // 通过角度计算弧度值，因为时钟的角度起线是y轴负方向，而View角度的起线是x轴正方向，所以要加270度
        float angle = 270 + 360 * percentage;

        float x = (float) (mHourPointerLength * Math.cos(Math.PI * 2 / 360 * angle));
        float y = (float) (mHourPointerLength * Math.sin(Math.PI * 2 / 360 * angle));

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    /**
     * 绘制分针
     */
    protected void drawMinutePointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mMinutePointerWidth);
        mPointerPaint.setColor(mColorMinutePointer);

        float s = mM * 60 + mS;
        float percentage = s / (60 * 60);
        float angle = 270 + 360 * percentage;

        float x = (float) (mMinutePointerLength * Math.cos(Math.PI * 2 / 360 * angle));
        float y = (float) (mMinutePointerLength * Math.sin(Math.PI * 2 / 360 * angle));

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    /**
     * 绘制秒针
     */
    protected void drawSecondPointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mSecondPointerWidth);
        mPointerPaint.setColor(mColorSecondPointer);

        float s = mS;
        float percentage = s / 60;
        float angle = 270 + 360 * percentage;

        float x = (float) (mSecondPointerLength * Math.cos(Math.PI * 2 / 360 * angle));
        float y = (float) (mSecondPointerLength * Math.sin(Math.PI * 2 / 360 * angle));

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    @Override
    public void move(int maxWidth, int maxHight) {
        getTime();
    }
}
