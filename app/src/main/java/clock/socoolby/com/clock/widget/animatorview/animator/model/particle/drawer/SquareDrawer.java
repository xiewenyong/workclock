package clock.socoolby.com.clock.widget.animatorview.animator.model.particle.drawer;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.I_PraticleDraw;
import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.Particle;

public class SquareDrawer implements I_PraticleDraw {
    public static  SquareDrawer getInstance(){
       return new SquareDrawer();
    }

    @Override
    public void draw(Canvas canvas, Particle particle, Paint paint) {
        paint.setAlpha(particle.alpha);
        paint.setColor(particle.color);
        canvas.drawRect(particle.x-particle.r,particle.y-particle.r,particle.x+particle.r,particle.y+particle.r,paint);
    }
}
