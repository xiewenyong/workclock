package clock.socoolby.com.clock;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.utils.FuncUnit;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.widget.wheelview.WheelView;
import clock.socoolby.com.clock.widget.wheelview.adapters.ArrayWheelAdapter;

public class SettingActivity extends Activity implements View.OnClickListener {

    public static final String[]  FAMOUS_QUOTES=new String[]{
            "人生在勤，不索何获？——张衡",
            "尊重人不应该胜于尊重真理。——柏拉图",
            "血沃中原肥劲草，寒凝大地发春华。——鲁迅",
            "读书百遍，其义自见。——陈遇",
            "真理是时间的女儿。——达·芬奇",
            "宽恕而不忘却，就如同把斧头埋在土里而把斧柄留在外面一样。——美国.巴斯克里",
            "君子喻于义，小人喻于利。——孔丘",
            "差以毫厘，谬以千里。——《汉书》",
            "它山之石，可以攻玉。——《诗经·小雅》",
            "从善如登，从恶如崩。——《国语》",
            "生活就是战斗。——柯罗连科",
            "一个伟大的人有两颗心：一颗心流血，一颗心宽容。——纪伯伦",
            "吃一堑，长一智。——古谚语",
            "锲而舍之，朽木不折；锲而不舍，金石可镂。——荀况",
            "天下事有难易乎，为之，则难者亦易矣;不为，则易者亦难矣。——彭端叔",
            "坚持真理的人是伟大的。——雨果",
            "星星之火，可以燎原。——尚书",
            "地不耕种，再肥沃也长不出果实；人不学习，再聪明也目不识丁。——西塞罗",
            "国耻未雪，何由成名？——李白",
            "操千曲而后晓声，观千剑而后识器。——刘勰",
            "辱，莫大于不知耻。——王通",
            "不满足是向上的车轮。——鲁迅",
            "出师未捷身先死，长使英雄泪沾襟。——杜甫",
            "度尽劫波兄弟在，相逢一笑泯恩仇。——鲁迅",
            "先天下之忧而忧，后天下之乐而乐。——范仲淹",
            "天意怜幽草，人间重晚晴。——李商隐",
            "土扶可城墙，积德为厚地。——李白",
            "忍耐是痛苦的，但它的结果是甜蜜的。——法.卢梭",
            "臣心一片磁针石，不指南方不肯休。——文天祥",
            "学问是苦根上长出来的甜果。——李嘉图",
            "学而不思则罔，思而不学则殆。——孔子",
            "私心胜者，可以灭公。——林逋",
            "不积跬步，无以至千里；不积小流，无以成江海。——荀况",
            "不责人小过，不发人阴私，不念人旧恶——三者可以养德，也可以远害。——洪应明",
            "学无止境。——荀子",
            "学而不厌，诲人不倦。——孔子",
            "天时不如地利，地利不如人和。——《孟子》",
            "春蚕到死丝方尽，蜡炬成灰泪始干。——李商隐",
            "心事浩茫连广宇，于无声处听惊雷。——鲁迅",
            "仓廪实则知礼节，衣食足则知荣辱。——《管子》",
            "深以刻薄为戒，每事当从忠厚。——薜渲",
            "春风得意马蹄疾，一日看尽长安花。——孟郊",
            "静以修身，俭以养德。——诸葛亮",
            "白日莫闲过，青春不再来。——林宽",
            "提出一个问题，往往比解决一个问题更重要。——爱因斯坦",
            "宽宏精神是一切事物中最伟大的。——欧文",
            "世界上最宽阔的是海洋，比海洋更宽阔的是天空，比天空更宽阔的是人的胸怀。——法.雨果",
            "天下兴亡，匹夫有责。——顾炎武",
            "宽容意味着尊重别人的任何信念。——爱因斯坦",
            "踏破铁鞋无觅处，得来全不费功夫。——《水浒传》",
            "上下同欲者胜。——孙武",
            "沉舟侧畔千帆过，病树前头万木春。——刘禹锡",
            "唯宽可以容人，唯厚可以载物。——薜渲",
            "读书破万卷，下笔如有神。——杜甫",
            "海纳百川有容乃大，山高万仞无欲则刚。——林则徐",
            "贫而无谄，富而无骄。——子贡",
            "君子忧道不忧贫。——孔丘",
            "有时宽容引起的道德震动比惩罚更强烈。——原苏联.苏霍姆林斯基",
            "志当存高远。——诸葛亮",
            "察己则可以知人，察今则可以知古。——《吕氏春秋》",
            "不会宽容别人的人，是不配受别人宽容的，但谁能说自己是不需要宽容的呢？——屠格涅夫",
            "天行有常，不为尧存，不为桀亡。——《苟子》",
            "采得百花成蜜后，为谁辛苦为谁甜。——罗隐",
            "信言不美，美言不信。善者不辩，辩者不善。——老子",
            "不知则问，不能则学。——董仲舒",
            "希望是厄运的忠实的姐妹。——普希金",
            "必须有勇气正视无情的真理。——列宁",
            "桃李不言，下自成蹊。——史记",
            "知耻近乎勇。——孔丘",
            "新沐者必弹冠，新浴者必振衣。——屈原",
            "小荷才露尖尖角，早有蜻蜓立上头。——杨万里",
            "只有人的劳动才是神圣的。——高尔基",
            "夕阳无限好，只是近黄昏。——李商隐",
            "燕雀安知鸿鹄之志哉！——陈涉",
            "人心不是靠武力征服，而是靠爱和宽容大度征服。——斯宾诺",
            "业精于勤，荒于嬉。——韩愈",
            "没有宽宏大量的心肠，便算不上真正的英雄。——俄.普希金",
            "春色满园关不住，一枝红杏出墙来。——叶绍翁",
            "要知天下事，须读古人书。——冯梦龙",
            "尺有所短，寸有所长。——屈原",
            "学然后知不足。——礼记",
            "长风破浪会有时，直挂云帆济沧海。——李白"
    };


    public static final String[] POETRY_OF_THE_TANG_DYNASTY=new String[]{
            "珠箔因风起，飞蛾入最能。不教人夜作，方便杀明灯。--【春夜裁缝】蒋维翰",
            "空山不见人，但闻人语响。返景入深林，复照青苔上。--鹿柴 唐代：王维",
            "千山鸟飞绝，万径人踪灭。孤舟蓑笠翁，独钓寒江雪。--江雪  唐代：柳宗元",
            "白日依山尽，黄河入海流。欲穷千里目，更上一层楼。--登鹳雀楼  唐代：王之涣",
            "驿外断桥边，寂寞开无主。已是黄昏独自愁，更著风和雨。无意苦争春，一任群芳妒。零落成泥碾作尘，只有香如故。--卜算子·咏梅 宋代：陆游",
            "昨夜雨疏风骤，浓睡不消残酒。试问卷帘人，却道海棠依旧。知否，知否？应是绿肥红瘦.--如梦令·昨夜雨疏风骤 宋代：李清照"
    };

    public static final String[] KONGZI=new String[]{
            "三人行，必有我师焉，择其善者而从之，择其不善者而改之。",
            "三军可夺帅也，匹夫不可夺志也！",
            "君子欲讷于言而敏于行。",
            "言未及之而言谓之躁，言及之而不言谓之隐，未见颜色而言谓之瞽。",
            "无欲速，无见小利。欲速，则不达；见小利，则大事不成。",
            "执德不弘，信道不笃，焉能为有，焉能为亡。",
            "过而不改，是谓过矣！",
            "好直不好学，其蔽也绞；好勇不好学，其蔽也乱；好刚不好学，其蔽也狂。",
            "人无远虑，必有近忧。",
            "君子食无求饱，居无求安，敏于事而慎于言，就有道而正焉，可谓好学也已。",
            "恭而无礼则劳，慎而无礼则葸，勇而无礼则乱，直而无礼则绞。",
            "己所不欲，匆施于人。",
            "居处恭，执事敬，与人忠。",
            "躬自厚而薄责于人，则远怨矣。",
            "与朋友交，言而有信。以文会友，以友辅仁。",
            "君子名之必可言也，言之必可行也，君子于其言，无所苟而已矣。",
            "君子义以为质，礼以行之，孙以出之，(www.lz13.cn)信以成之。君子哉！",
            "孔子曰：“能行五者于天下为仁矣。”请问之。曰：“恭宽信敏惠。恭则不侮，宽则得众，信则人任焉，敏则有功，惠则足以使人”。",
            "益者三友，损者三友。友直，友谅，友多闻，益矣。友便辟，友善柔，友便佞，损矣。",
            "可与言而不与之言，失人；不可与言而与之言，失言。知者不失人，亦不失言。",
            "过，则匆惮改。",
            "士不可不弘毅，任重而道远。仁以为己任，不亦重乎？死而后己，不亦远乎？",
            "君子敬而无失，与人恭而有礼，四海之内皆兄弟也，言忠信，行笃敬，虽蛮貊之邦，行矣。言不忠信，行不笃敬，虽州里，行乎哉？",
            "以文会友，以友辅仁。",
            "行己有耻，使于四方，不辱君命，可谓士矣。",
            "己欲立而立人，己欲达而达人。",
            "见贤思齐焉，见不贤而内自省也。"
    };

    public static final String[] LAOZI=new String[]{
            "太上，不知有之；其次，亲而誉之；其次，畏之；其次，侮之。信不足焉，有不信焉。",
            "宠辱若惊，贵大患若身。",
            "江海所以能为百谷王者，以其善下之。——《道德经》第六十六章",
            "金玉满堂，莫之能守。富贵而骄，自遗其咎。功遂身退，天之道。——《道德经》第九章",
            "五色令人目盲；五音令人耳聋；五味令人口爽；驰骋畋猎，令人心发狂；难得之货，令人行妨。是以圣人为腹不为目，故去彼取此。",
            "人之所畏，不可不畏。",
            "上善若水，水善利万物而不争，处众人之所恶，故几于道。居善地，心善渊，与善仁，言善信，正善治，事善能，动善时。",
            "天下皆知美之为美，斯恶已，皆知善之为善，斯不善已……",
            "居善地，心善渊，与善仁，言善信，政善治，事善能，动善时。夫唯不争，故无尤。",
            "持而盈之，不如其已；揣而锐之，不可长保。金玉满堂，莫之能守；富贵而骄，自遗其咎。功遂身退，天之道也。",
            "上德不德，是以有德；下德不失德，是以无德。",
            "三十辐，共一毂，当其无，有车之用。埏埴以为器，当其无，有器之用。凿户牖以为室，当其无，有室之用。故有之以为利，无之以为用。",
            "天网恢恢，疏而不失。——《道德经》第七十三章",
            "天下皆知美之为美，斯恶矣；皆知善之为善，斯不善已。——《道德经》第二章",
            "知人者智，自知者明；胜人者有力，自胜者强。",
            "俗人昭昭，我独昏昏。俗人察察，我独闷闷。",
            "不出户，知天下；不窥牖，见天道。",
            "天之道，损有余而补不足。人之道，则不然，损不足以奉有余。",
            "圣人自知不自见；自爱不自贵。——《道德经》第七十二章",
            "绝圣弃智，民利百倍；绝仁弃义，民复孝慈；绝巧弃利，盗贼无有。此三者以为文，不足。故令有所属：见素抱朴，少思寡欲，绝学无忧。",
            "是以圣人不行而知，不见而名，不为而成。",
            "希言自然。——《道德经》第二十三章",
            "人法地，地法天，天法道，道法自然。",
            "夫唯不争，故天下莫能与之争。",
            "天道无亲，常与善人。",
            "大道废，有仁义；智慧出，有大伪；六亲不和，有孝慈；国家昏乱，有正臣。——《道德经》第十八章",
            "道可道，非常道。名可名，非常名。",
            "大道废，有仁义；智慧出，有大伪；六亲不和，有孝慈；国家昏乱，有忠臣。",
            "天地不仁，以万物为刍狗；圣人不仁，以百姓为刍狗。——《道德经》第五章",
            "曲则全，枉则直，洼则盈，敝则新，少则多，多则惑。",
            "天地所以能长且久者，以其不自生，故能长生。——《道德经》第七章",
            "民不畏死，奈何以死惧之。——《道德经》第七十四章",
            "大成若缺，其用不弊；大盈若冲，其用不穷。大直若屈，大巧若拙，大辫若讷。",
            "绝圣弃智，民利百倍；绝仁弃义，民复孝慈；绝巧弃利，盗贼无有。——《道德经》第十九章",
            "民之难治，以其智多。故以智治国，国之贼。不以智治国，国之福。——《道德经》第六十五章"
    };

    public static String roundAutoQuotes(){
        int index=new java.util.Random().nextInt(4);
        switch (index){
            case 1:
                return roundTangShi();
            case 2:
                return roundLaoZi();
            case 3:
                return roundKonZi();
            default:
                return roundFamousQuotes();
        }
    }

    public static String roundFamousQuotes(){
        return roundFamousQuotes(FAMOUS_QUOTES);
    }

    public static String roundLaoZi(){
        return roundFamousQuotes(LAOZI);
    }

    public static String roundKonZi(){
        return roundFamousQuotes(KONGZI);
    }

    public static String roundTangShi(){
        return roundFamousQuotes(POETRY_OF_THE_TANG_DYNASTY);
    }

    public static String roundFamousQuotes(String[] arrays){
        int index=new java.util.Random().nextInt(arrays.length);
        return arrays[index];
    }


    private WheelView weel_startTime;
    private WheelView weel_stopTime;
    RadioButton rb_halfhour;
    RadioButton rb_hours;
    RadioButton rb_noreport;
    private EditText et_description;
    private EditText et_city;


    private SharePerferenceModel model;

    String[] listTime = new String[48];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        RadioGroup rg_taking_clock = (RadioGroup) findViewById(R.id.rg_talking_clock);
        rb_halfhour = (RadioButton) findViewById(R.id.rb_halfhour);
        rb_hours = (RadioButton) findViewById(R.id.rb_hours);
        rb_noreport = (RadioButton) findViewById(R.id.rb_noreport);
        weel_startTime = (WheelView) findViewById(R.id.weel_start_time);
        weel_stopTime = (WheelView) findViewById(R.id.weel_stop_time);

        et_city = (EditText) findViewById(R.id.et_city);
        et_description = (EditText) findViewById(R.id.et_description);
        model = new SharePerferenceModel();
        model.read();

        CheckBox cb_tick = (CheckBox) findViewById(R.id.cb_tick);
        cb_tick.setChecked(model.isTickSound());
        cb_tick.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                model.setTickSound(b);
            }
        });
        CheckBox cb_trigger_screen = (CheckBox) findViewById(R.id.cb_trigger_screen);
        cb_trigger_screen.setChecked(model.isTriggerScreen());
        cb_trigger_screen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                model.setTriggerScreen(isChecked);
            }
        });

        Button btn_uninstall = (Button) findViewById(R.id.btn_uninstall);
        btn_uninstall.setOnClickListener(this);
        Button btn_about = (Button) findViewById(R.id.btn_about);
        btn_about.setOnClickListener(this);


        for (int i = 0; i < 48; i++) {
            int hours = i / 2;
            int minutes = i % 2 * 30;
            String timeString = String.format("%02d:%02d", hours, (minutes + 1));
            listTime[i] = timeString;
        }

        ArrayWheelAdapter<String> timeAdpater = new ArrayWheelAdapter<String>(this, listTime);
        weel_startTime.setViewAdapter(timeAdpater);
        ArrayWheelAdapter<String> durationAdapter = new ArrayWheelAdapter<String>(this, listTime);
        weel_stopTime.setViewAdapter(durationAdapter);
        initData();

        rg_taking_clock.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int index) {
                int checkID = radioGroup.getCheckedRadioButtonId();
                switch (checkID) {
                    case R.id.rb_halfhour:
                        model.setTypeHourPower(Constants.TALKING_HALF_AN_HOUR);
                        break;
                    case R.id.rb_hours:
                        model.setTypeHourPower(Constants.TALKING_HOURS);
                        break;
                    case R.id.rb_noreport:
                        model.setTypeHourPower(Constants.TALKING_NO_REPORT);
                        break;

                }

            }
        });
    }

    private void initData() {
        int startTimeIndex = indexOfTimeString(String.format("%02d:%02d", model.getStartHourPowerTime().getHour(), model.getStartHourPowerTime().getMinute()));
        int stopTimeIndex = indexOfTimeString(String.format("%02d:%02d", model.getStopHourPowerTime().getHour(), model.getStopHourPowerTime().getMinute()));
        weel_startTime.setCurrentItem(startTimeIndex);
        weel_stopTime.setCurrentItem(stopTimeIndex);


        switch (model.getTypeHourPower()) {
            case Constants.TALKING_HALF_AN_HOUR:
                rb_halfhour.setChecked(true);
                break;
            case Constants.TALKING_HOURS:
                rb_hours.setChecked(true);
                break;
            case Constants.TALKING_NO_REPORT:
                rb_noreport.setChecked(true);
                break;
        }
        et_city.setText(model.getCity());
        et_description.setText(model.getDescription());
    }

    @Override
    protected void onPause() {
        super.onPause();
        reportTimeConfirm();
        model.setCity(et_city.getText().toString());
        model.setDescription(et_description.getText().toString());
        model.save();
        setResult(Constants.SUCCESS_CODE);
        finish();
    }

    private int indexOfTimeString(String timeString) {
        for (int i = listTime.length - 1; i >= 0; i--) {
            if (listTime[i].equals(timeString))
                return i;
        }
        return 0;
    }

    private void reportTimeConfirm() {
        String timeStart = listTime[weel_startTime.getCurrentItem()];
        String timeStop = listTime[weel_stopTime.getCurrentItem()];

        DateModel startTimeModel = new DateModel();
        startTimeModel.setTimeString(timeStart);
        DateModel stopTimeModel = new DateModel();
        stopTimeModel.setTimeString(timeStop);
        model.setStartHourPowerTime(startTimeModel);
        model.setStopHourPowerTime(stopTimeModel);
    }


    private void uninstallActivity() {
        DevicePolicyManager policyManager;
        ComponentName componentName;
        policyManager = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);

        componentName = new ComponentName(this, ActivateAdmin.class);
        policyManager.removeActiveAdmin(componentName);
        startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + FuncUnit.getBoxPackageName())));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_uninstall:
                uninstallActivity();
                break;
            case R.id.btn_about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
        }
    }

}
