package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//引用自：https://github.com/xianfeng99/Particle

public class SnowAnimator extends AbstractAnimator<SnowAnimator.SnowPoint> {

    public SnowAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public SnowAnimator() {
        super(50);
    }


    @Override
    public SnowPoint createNewEntry() {
        int round=rand.nextInt(2);
        if(randColor)
            randomColorIfAble();
        if(round==1)
            return new SnowPoint2(color);
        return new SnowPoint(color);
    }

    public class SnowPoint implements I_AnimatorEntry {
        int color;
        protected final int size = 36; // 长度在0-50像素
        private Rect rect; // 雪图
        protected Point point; // 雪点
        protected Point speed; // 雪点x,y方向速度


        public SnowPoint(int color) {
            rect = new Rect();
            speed = new Point();
            point=new Point();
            reset();
            this.color=color;
        }

        int count = 0;


        public void changeSpeed(){
            count++;
            if (count > 5) {
                count = 0;
                speed.x = rand.nextInt(size);
                speed.x = speed.x > speed.y ? speed.y : speed.x;
                speed.x = rand.nextBoolean() ? -speed.x : speed.x;
                speed.y += rand.nextBoolean() ? 1 : 0;
            }
        }


        public boolean mustReset(){
            if (rect.left < 0 || rect.bottom > height) {
                return true;
            }
            return false;
        }

        public void reset() {
           reset(true);
        }

        public void reset(boolean resetRect) {
            point.x = rand.nextInt(width);
            point.y = rand.nextInt(height);


            //减少出现在下面的机率
            if(point.y>height*2/3&&rand.nextBoolean())
                point.y=point.y-height/2;

            if(resetRect) {
                int w = rand.nextInt(size);
                int h = rand.nextInt(size);

                if (w > 8) {
                    //勾３股４弦５（宽是４的倍数，高是３的倍数）
                    int mod = w % 4;
                    w += mod;
                    int mul = w / 4;//倍数
                    h = 3 * mul;
                    rect.left = point.x;
                    rect.top = point.y;
                    rect.right = point.x + w;
                    rect.bottom = point.y + h;
                } else {
                    rect.left = point.x;
                    rect.top = point.y;
                    rect.right = point.x + w;
                    rect.bottom = point.y + w;
                }
            }

            //修改：速度过快
            int speedX = rand.nextInt(size/2);
            int speedY = rand.nextInt(size/2);

            /*
             * int speedX = w; int speedY = h;
             */

            speedX = speedX == 0 ? 1 : speedX;
            speedY = speedY == 0 ? 1 : speedY;
            speedX = speedX > speedY ? speedY : speedX;

            speed.x = speedX;
            speed.y = speedY;
        }


        public void drawSknow(Canvas canvas,Paint paint){
            int w = rect.width();
            int h = rect.height();
            int mul = w / 4;//倍数
            float xie = 5 * mul / 2;
            float centerY = rect.top + h / 2;
            float centerX = rect.left + w / 2;

            canvas.drawLine(rect.left, rect.top, rect.right, rect.bottom, paint);
            canvas.drawLine(rect.left, rect.bottom, rect.right, rect.top, paint);
            canvas.drawLine(centerX, centerY - xie, centerX, centerY + xie, paint);

        }

        public void printPosition() {
            Log.d("SknowPoint", "x : " + rect.left + " y : " + rect.top + " r : "
                    + rect.right + " b : " + rect.bottom);
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            point.x += speed.x;
            point.y += speed.y;
            rect.left += point.x;
            rect.top += point.y;
            rect.right = rect.right + speed.x;
            rect.bottom = rect.bottom + speed.y;

            changeSpeed();

            if (mustReset()) {
                reset();
            }
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            mPaint.setColor(color);
            //变长小于等于８绘制圆形
            if(rect.width() <= 8){
                canvas.drawCircle(rect.left, rect.top, rect.width() / 2, mPaint);
            }
            else{
                //绘制雪花形状
                drawSknow(canvas,mPaint);
            }
        }
    }

    public class SnowPoint2 extends SnowPoint{
        private int lenSize;// 边长
        private int mul;// 倍数

        public SnowPoint2(int color) {
           super(color);
        }

        public void reset() {
            super.reset(false);
            lenSize = rand.nextInt(size);
            lenSize += lenSize % 5;
            mul = lenSize / 5;
        }

        @Override
        public boolean mustReset() {
            if (point.x < 0 || point.x > width || point.y > height) {
                return true;
            }
            return false;
        }

        @Override
        public void onDraw(Canvas canvas,Paint paint) {
            paint.setColor(color);
            // 变长小于等于10绘制圆形
            if (lenSize <= 10) {
                canvas.drawCircle(point.x, point.y, lenSize / 2, paint);
            } else {
                // 绘制雪花形状
                drawSknow(canvas,paint);
            }
        }

        public void drawSknow(Canvas canvas, Paint paint) {
            int y = mul * 3;
            int x = mul * 4;

            if (speed.x > 0) {
                // 竖雪花
                canvas.drawLine(point.x, point.y - (float) lenSize / 2, point.x,
                        point.y + (float) lenSize / 2, paint);
                canvas.drawLine(point.x - (float) x / 2, point.y - (float) y / 2,
                        point.x + (float) x / 2, point.y + (float) y / 2, paint);
                canvas.drawLine(point.x - (float) x / 2, point.y + (float) y / 2,
                        point.x + (float) x / 2, point.y - (float) y / 2, paint);
            } else {
                // 横雪花
                canvas.drawLine(point.x - (float) lenSize / 2, point.y, point.x
                        + (float) lenSize / 2, point.y, paint);
                canvas.drawLine(point.x - (float) y / 2, point.y - (float) x / 2,
                        point.x + (float) y / 2, point.y + (float) x / 2, paint);
                canvas.drawLine(point.x - (float) y / 2, point.y + (float) x / 2,
                        point.x + (float) y / 2, point.y - (float) x / 2, paint);
            }
        }
    }
}
