package clock.socoolby.com.clock.widget.animatorview.animator.model.speed;

import com.daimajia.easing.BaseEasingMethod;
import com.daimajia.easing.Skill;

public class EaseLineInterpolator extends AbstractLineInterpolator {

    BaseEasingMethod baseEasingMethod;
    public EaseLineInterpolator(Float base, Float maxValue, Skill skill) {
        super(base, maxValue);
        baseEasingMethod=skill.getMethod(maxValue-base);
    }

    @Override
    public Float calculate(float timeGo, float base, float lenght, float totalTime) {
        return baseEasingMethod.calculate(timeGo,base,lenght,totalTime);
    }
}
