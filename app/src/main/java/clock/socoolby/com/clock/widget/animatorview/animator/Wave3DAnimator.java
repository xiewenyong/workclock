package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//参考：http://www.jsdaima.com/js/2689.html
public class Wave3DAnimator extends AbstractAnimator<Wave3DAnimator.Wave> {
    public static final String Tag=Wave3DAnimator.class.getSimpleName();

    public Wave3DAnimator() {
        super(1);
    }

    @Override
    public Wave createNewEntry() {
        int count=rand.nextInt(50);
        if(count<20)
            count=count+20;
        int steps=rand.nextInt(30);
        if(steps<5)
            steps=steps+5;
        return new Wave(width,height,count,steps);
    }

    public class Wave implements I_AnimatorEntry {
        private int width;
        private int height;
        private float midX;
        private float midY;
        private int count;
        private int steps;
        private int tick;
        private List<PointCicle> datas;

        public Wave(int width, int height,int count,int steps) {
            this.width = width;
            this.height = height;
            this.count =count;
            this.steps =steps;
            initWave();
        }

        public void initWave() {

            this.midX =this.width /2;
            this.midY =this.height /2;
            this.tick  =0;
            this.datas = new ArrayList();

            float sRel, sMax, r;
            for (int s = 0; s < steps; s++) {
                sRel = s*1.0f / steps;
                sMax = sRel * count;
                //randomColorIfAble();
                for (int i = 0; i < sMax; i++) {
                    r = new Double((rand.nextInt(100)/100 - 0.5) * i / sMax / 10).intValue();
                    PointCicle pointCicle=new PointCicle();
                    pointCicle.x=new Double(Math.cos(r + i / sMax * Math.PI * 2) * sRel).floatValue();
                    pointCicle.y=new Double(Math.sin(r + i / sMax * Math.PI * 2) * sRel).floatValue();
                    pointCicle.d=sRel;
                    pointCicle.color=color;
                    datas.add(pointCicle);
                }
            }
            Log.d(Tag,"initWave datas count:"+datas.size());
        }


        @Override
        public void move(int maxWidth, int maxHight) {
            this.tick--;
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {

            for(PointCicle point:datas){
                // const h  = Math.sin(this.tick * 0.1 + point.d * (Math.sin(this.tick * 0.05) + 1) * 2);
                float h  = new Double(Math.sin(this.tick * 0.1 + point.d * 7.5)).floatValue();
                float hl = new Double(Math.sin(this.tick * 0.12 + point.d * 7.5)).floatValue();
                float hs = new Double(h * this.height * 0.1).floatValue();
                float s  = new Double(0.5 + (point.y + 1) * 2).floatValue();

                //this.engine.fillStyle = `hsla(${180 + hl * 180 | 0}, 70%, ${25 + (h * -1) * 25 | 0}%, ${0.5 + (h *-1) * 0.5})`;

                // this.engine.fillRect(
                //     this.midX + point.x * this.width * 0.3,
                //     this.midY + point.y * this.width * 0.1 + hs,
                //     3 * s,
                //     3 * s
                // );

                mPaint.setColor(point.color);

                canvas.drawCircle(new Double(this.midX + point.x * this.width * 0.3).floatValue(),new Double(this.midY + point.y * this.width * 0.1 + hs).floatValue(),new Double(1.5 * s).floatValue(),mPaint);

            }
        }
    }

    public class PointCicle{
        float x,y,d;
        int color;
        public PointCicle() {
        }

        public PointCicle(float x, float y, float d) {
            this.x = x;
            this.y = y;
            this.d = d;
        }
    }
}
