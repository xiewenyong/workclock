package clock.socoolby.com.clock.widget.animatorview;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface I_AnimatorEntry {

    void move(int maxWidth, int maxHight);

    void onDraw(Canvas canvas, Paint mPaint);
}
