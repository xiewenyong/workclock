package clock.socoolby.com.clock.widget.animatorview.animator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;


//参考自：https://github.com/Q42/AndroidScrollingImageView
public class ScrollingImageAnimator extends AbstractAnimator<ScrollingImageAnimator.ScrollingImage> {

    public ScrollingImageAnimator() {
        super(DYNAMIC_QUANTITY);
    }

    public void addResourceIds(int[] resourceIds,int speed,boolean randStartX){
        for(int i=0;i<resourceIds.length;i++)
           list.add(new ScrollingImage(speed,resourceIds[i],rand.nextInt(width),rand.nextInt(height),randStartX));
    }

    public void addBackgroundResourceId(int resourceId,int left,int top){
        list.add(new ScrollingImage(0,resourceId,left,top,false));
    }

    @Override
    public ScrollingImage createNewEntry() {
        return null;
    }

    public class ScrollingImage implements I_AnimatorEntry {
        private Bitmap bitmap;
        private float speed;
        private float offset;
        private boolean isStarted;
        int resourceID;
        private int startX,startY;
        boolean randStartX;

        public ScrollingImage(float speed,int resourceID,int startX,int startY,boolean randStartX) {
            this.speed = speed;
            this.resourceID=resourceID;
            this.startX=startX;
            offset=startX;
            this.startY=startY;
            this.randStartX=randStartX;
            init();
        }

        private void init(){
            bitmap = loadBitmap(mainView.getContext(), resourceID);
        }

        /**
         * Start the animation
         */
        public void start() {
            if (!isStarted) {
                isStarted = true;
            }
        }

        /**
         * Stop the animation
         */
        public void stop() {
            if (isStarted) {
                isStarted = false;
            }
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            if (isStarted && speed != 0) {
                offset -= Math.abs(speed);
                if(offset<-bitmap.getWidth()) {
                    offset = width;
                    if(randStartX) {
                        offset = rand.nextInt(width);
                    }
                }
            }
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            canvas.drawBitmap(bitmap, offset, startY, null);
        }
    }


   public static Bitmap loadBitmap(Context context, int resourceId) {
            return BitmapFactory.decodeResource(context.getResources(), resourceId);
    }
}
