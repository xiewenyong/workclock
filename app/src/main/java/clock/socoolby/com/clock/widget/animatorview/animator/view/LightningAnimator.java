package clock.socoolby.com.clock.widget.animatorview.animator.view;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//参考：https://www.jianshu.com/p/0dda922897cf
public class LightningAnimator extends AbstractAnimator<LightningAnimator.Lightning> {

    public LightningAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public LightningAnimator() {
        super(1);
    }

    @Override
    public Lightning createNewEntry() {
        return new Lightning(0,0,width,height);
    }

    public class Lightning implements I_AnimatorEntry {
        private Shader mGradient;
        private Matrix mGradientMatrix;
        private Paint mPaint1;
        private int mViewWidth = 0, mViewHeight = 0;
        private float mTranslateX = 0, mTranslateY = 0;
        private boolean mAnimating = true;
        private Rect rect;

        public Lightning(int left,int top,int mViewWidth,int mViewHeight) {
            this.mViewWidth = mViewWidth;
            this.mViewHeight = mViewHeight;
            rect.set(left, top, mViewWidth, mViewHeight);
            init();
        }

        private void init() {
            rect = new Rect();
            mPaint1 = new Paint();

            if (mViewWidth > 0) {
                //亮光闪过
                mGradient = new LinearGradient(0, 0, mViewWidth / 2, mViewHeight,
                        new int[]{0x00ffffff, 0x73ffffff, 0x00ffffff,  0x99ffffff, 0x00ffffff},
                        new float[]{0.2f,       0.35f,      0.5f,        0.7f,      1},
                        Shader.TileMode.CLAMP);
                mPaint1.setShader(mGradient);
                mPaint1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.LIGHTEN));
                mGradientMatrix = new Matrix();
                mGradientMatrix.setTranslate(-2 * mViewWidth, mViewHeight);
                mGradient.setLocalMatrix(mGradientMatrix);
            }
        }

        float step =0;//0-1

        @Override
        public void move(int maxWidth, int maxHight) {
            step = step +0.1f;
            if(step >1)
                step =0;
            //❶ 改变每次动画的平移x、y值，范围是[-2mViewWidth, 2mViewWidth]
            mTranslateX = 4 * mViewWidth * step - mViewWidth * 2;
            mTranslateY = mViewHeight * step;
            //❷ 平移matrix, 设置平移量
            if (mGradientMatrix != null) {
                mGradientMatrix.setTranslate(mTranslateX, mTranslateY);
            }
            //❸ 设置线性变化的matrix
            if (mGradient != null) {
                mGradient.setLocalMatrix(mGradientMatrix);
            }
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            if (mAnimating && mGradientMatrix != null) {
                canvas.drawRect(rect, mPaint1);
            }
        }
    }
}
