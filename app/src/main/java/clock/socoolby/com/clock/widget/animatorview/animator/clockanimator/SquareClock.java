package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//方型
public class SquareClock extends AbstractClock {

    /**
     * 根据控件的大小，初始化时钟刻度的长度和宽度、指针的长度和宽度、时钟中心点的半径
     */
    protected void initClockPointerLength() {
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        mDefaultScaleLength = mRadius / 10;
        mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        mParticularlyScaleLength = mRadius / 5;
        mParticularlyScaleWidth = mParticularlyScaleLength / 6;

        /*
         * 时针长=半径/3;
         * 时针宽=特殊时钟刻度宽;
         *
         * */
        mHourPointerLength = mRadius / 3;
        mHourPointerWidth = mParticularlyScaleWidth;

        /*
         * 分针长=半径/2;
         * 分针宽=特殊时钟刻度宽;
         *
         * */
        mMinutePointerLength = mRadius / 2;
        mMinutePointerWidth = mParticularlyScaleWidth;

        /*
         * 秒针长=半径/3*2;
         * 秒针宽=默认时钟刻度宽;
         *
         * */
        mSecondPointerLength = mRadius / 3 * 2;
        mSecondPointerWidth = mDefaultScaleWidth;

        // 中心点半径=（默认刻度宽+特殊刻度宽）/2
        mPointRadius = (mDefaultScaleWidth + mParticularlyScaleWidth) / 2;
    }

    /**
     * 绘制时钟的外型形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
        mDefaultPaint.setColor(mClockColor);

        canvas.drawRect(0, 0,width,hight, mDefaultPaint);

            float radianTanValue=0f;
            int j=1;
            float stepLeng=0;
            float startX=mCenterX;
            float scaleLength=0;
            mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
            mDefaultPaint.setColor(mColorParticularyScale);

            canvas.drawLine(startX, 0, startX, 0 + mParticularlyScaleLength, mDefaultPaint);
            canvas.drawLine(startX, hight- mParticularlyScaleLength,startX, hight,  mDefaultPaint);

            float floatLength=0;
            while (stepLeng<width/2){//上下面
                radianTanValue=new Double(Math.tan(Math.toRadians(6 *j))).floatValue();
                stepLeng=radianTanValue*hight/2;

                if (j % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    scaleLength=mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    scaleLength=mDefaultScaleLength;
                }
                floatLength=radianTanValue*scaleLength;
                canvas.drawLine(startX+stepLeng, 0, startX+stepLeng-floatLength, 0 + scaleLength, mDefaultPaint);
                canvas.drawLine(startX-stepLeng, 0, startX-stepLeng+floatLength, 0 + scaleLength, mDefaultPaint);

                canvas.drawLine(startX+stepLeng, hight, startX+stepLeng-floatLength, hight- scaleLength, mDefaultPaint);
                canvas.drawLine(startX-stepLeng, hight, startX-stepLeng+floatLength, hight- scaleLength, mDefaultPaint);
                j++;
            }

         j=1;
         stepLeng=0;
         float  startY=mCenterY;

        mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
        mDefaultPaint.setColor(mColorParticularyScale);

        canvas.drawLine(0, startY, mParticularlyScaleLength, startY, mDefaultPaint);
        canvas.drawLine(width, startY, width- mParticularlyScaleLength, startY, mDefaultPaint);

        while (stepLeng<hight/2){//左右面
            radianTanValue=new Double(Math.tan(Math.toRadians(6 *j))).floatValue();
            stepLeng=radianTanValue*width/2;
            if (j % 5 == 0) { // 特殊时刻
                mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                mDefaultPaint.setColor(mColorParticularyScale);
                scaleLength=mParticularlyScaleLength;
            } else {          // 一般时刻
                mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                mDefaultPaint.setColor(mColorDefaultScale);
                scaleLength=mDefaultScaleLength;
            }
            floatLength=radianTanValue*scaleLength;
            canvas.drawLine(0, startY+stepLeng, 0 + scaleLength,startY+stepLeng-floatLength, mDefaultPaint);
            canvas.drawLine(width, startY+stepLeng, width- scaleLength,startY+stepLeng-floatLength, mDefaultPaint);

            canvas.drawLine(0,startY-stepLeng, 0 + scaleLength,startY-stepLeng+floatLength, mDefaultPaint);
            canvas.drawLine(width,startY-stepLeng, width - scaleLength,startY-stepLeng+floatLength, mDefaultPaint);
            j++;
        }
        //}
    }



    /**
     * 计算线段的起始坐标
     *
     * @param angle
     * @param length
     * @return
     */
    private float[] calculatePoint(float angle, float length) {
        int POINT_BACK_LENGTH = 1;
        float[] points = new float[4];
        if (angle <= 90f) {
            points[0] = -(float) Math.sin(angle * Math.PI / 180) * POINT_BACK_LENGTH;
            points[1] = (float) Math.cos(angle * Math.PI / 180) * POINT_BACK_LENGTH;
            points[2] = (float) Math.sin(angle * Math.PI / 180) * length;
            points[3] = -(float) Math.cos(angle * Math.PI / 180) * length;
        } else if (angle <= 180f) {
            points[0] = -(float) Math.cos((angle - 90) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[1] = -(float) Math.sin((angle - 90) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[2] = (float) Math.cos((angle - 90) * Math.PI / 180) * length;
            points[3] = (float) Math.sin((angle - 90) * Math.PI / 180) * length;
        } else if (angle <= 270f) {
            points[0] = (float) Math.sin((angle - 180) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[1] = -(float) Math.cos((angle - 180) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[2] = -(float) Math.sin((angle - 180) * Math.PI / 180) * length;
            points[3] = (float) Math.cos((angle - 180) * Math.PI / 180) * length;
        } else if (angle <= 360f) {
            points[0] = (float) Math.cos((angle - 270) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[1] = (float) Math.sin((angle - 270) * Math.PI / 180) * POINT_BACK_LENGTH;
            points[2] = -(float) Math.cos((angle - 270) * Math.PI / 180) * length;
            points[3] = -(float) Math.sin((angle - 270) * Math.PI / 180) * length;
        }
        return points;
    }


}
