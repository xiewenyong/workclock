package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

public class CircleClock extends AbstractClock {


    /**
     * 根据控件的大小，初始化时钟刻度的长度和宽度、指针的长度和宽度、时钟中心点的半径
     */
    protected void initClockPointerLength() {
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        mDefaultScaleLength = mRadius / 10;
        mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        mParticularlyScaleLength = mRadius / 5;
        mParticularlyScaleWidth = mParticularlyScaleLength / 6;

        /*
         * 时针长=半径/3;
         * 时针宽=特殊时钟刻度宽;
         *
         * */
        mHourPointerLength = mRadius / 3;
        mHourPointerWidth = mParticularlyScaleWidth;

        /*
         * 分针长=半径/2;
         * 分针宽=特殊时钟刻度宽;
         *
         * */
        mMinutePointerLength = mRadius / 2;
        mMinutePointerWidth = mParticularlyScaleWidth;

        /*
         * 秒针长=半径/3*2;
         * 秒针宽=默认时钟刻度宽;
         *
         * */
        mSecondPointerLength = mRadius / 3 * 2;
        mSecondPointerWidth = mDefaultScaleWidth;

        // 中心点半径=（默认刻度宽+特殊刻度宽）/2
        mPointRadius = (mDefaultScaleWidth + mParticularlyScaleWidth) / 2;
    }
    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);
        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
        mDefaultPaint.setColor(mClockColor);

        canvas.drawCircle(0, 0, mRadius, mDefaultPaint);

        for (int i = 0; i < 60; i++) {
            if (i % 5 == 0) { // 特殊时刻

                mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                mDefaultPaint.setColor(mColorParticularyScale);

                canvas.drawLine(0, -mRadius, 0, -mRadius + mParticularlyScaleLength, mDefaultPaint);

            } else {          // 一般时刻

                mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                mDefaultPaint.setColor(mColorDefaultScale);

                canvas.drawLine(0, -mRadius, 0, -mRadius + mDefaultScaleLength, mDefaultPaint);

            }
            canvas.rotate(6);
        }
        canvas.restore();
    }


}
