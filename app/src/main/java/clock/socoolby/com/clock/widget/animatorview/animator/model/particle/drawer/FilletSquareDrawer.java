package clock.socoolby.com.clock.widget.animatorview.animator.model.particle.drawer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;

import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.I_PraticleDraw;
import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.Particle;

public class FilletSquareDrawer implements I_PraticleDraw {
    public static  FilletSquareDrawer getInstance() {
        return new FilletSquareDrawer();
    }

    @Override
    public void draw(Canvas canvas, Particle particle, Paint paint) {
        paint.setAlpha(particle.alpha);
        paint.setColor(particle.color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect(particle.x - particle.r, particle.y - particle.r, particle.x + particle.r, particle.y + particle.r, particle.r / 2, particle.r / 2, paint);
        }else
            canvas.drawRect(particle.x-particle.r,particle.y-particle.r,particle.x+particle.r,particle.y+particle.r,paint);
    }
}