package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//参考：https://blog.csdn.net/u014112893/article/details/78556098
public class WindmillAnimator extends AbstractAnimator<WindmillAnimator.Windmill> {


    public WindmillAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public WindmillAnimator() {
       this(3);
    }

    @Override
    //功能由initEntryList()接管
    public Windmill createNewEntry() {
        return null;
    }

    @Override
    protected void initEntryList() {
        addWindmill(entryQuantity);
    }

    private void addWindmill(int quantity){
        //用于个个之间保证有足够的间距，不会都在一起
        int step=width/quantity;

        for(int i=0;i<quantity;i++) {
            int x=step*i+rand.nextInt(step);
            int y=rand.nextInt(height);
            int h=rand.nextInt(height-y);
            if(h<height/3) {
                h = h + height/3;
            }
            if(y+h>height)
                y=height-h-30;
            int w=h*3/4;
            if(x+w>width)
                x=width-w-30;
            if(randColor)
                randomColorIfAble();
            list.add(new Windmill(x,y,h,w,color));
        }
    }

    public class Windmill implements I_AnimatorEntry {
        private Paint mPaint;
        private Path mPath;
        private int width, height;
        private int x,y;
        int color;

        private  int lineWidth = 5;
        private  int windmillLeng; // 扇叶长度
        private  float lengthOfSide; // 扇叶小三角形的斜边长
        private  float angle = 0F;

        private int delay=0;//使个个转动都不同

        public Windmill(int x,int y,int width, int height,int color) {
            this.width = width;
            this.height = height;
            this.x=x;
            this.y=y;
            this.color=color;
            delay=rand.nextInt(5);
            initPaint();
        }

        private void initPaint() {
            mPaint = new Paint();
            mPath = new Path();
            mPaint.setColor(color);
            mPaint.setStrokeWidth(lineWidth);
            mPaint.setAntiAlias(true);
            windmillLeng = Math.min(width, height) / 2 - 20;
            lengthOfSide = (float) (windmillLeng / 10 / Math.cos(45));
        }


        // 每个扇叶由2个三角形组成，小三角形是由2个等边直角三角形组成,变成是扇叶长度的十分之一
        private void drawWindMills(Canvas canvas) {
            for (int i = 0; i < 3; i++) {
                //Log.i("drawWindMills", height + " / " + windmillLeng);
                mPath.moveTo(x+width / 2, y+ windmillLeng);
                mPath.lineTo(x+width / 2 + lengthOfSide * (float) Math.sin(Math.PI / 180 * (-45 + angle + 120 * i)),
                        y+ windmillLeng - lengthOfSide * (float) Math.cos(Math.PI / 180 * (-45 + angle + 120 * i)));
                mPath.lineTo(x+width / 2 + windmillLeng * (float) Math.sin(Math.PI / 180 * (120 * i + angle)),
                        y+ windmillLeng - windmillLeng * (float) Math.cos(Math.PI / 180 * (120 * i + angle)));
                mPath.lineTo(x+width / 2 + lengthOfSide * (float) Math.sin(Math.PI / 180 * (45 + angle + 120 * i)),
                        y+ windmillLeng - lengthOfSide * (float) Math.cos(Math.PI / 180 * (45 + angle + 120 * i)));
                mPath.lineTo(x+width / 2, y+ windmillLeng);
                mPath.close();
                canvas.drawPath(mPath, mPaint);
            }
            mPath.reset();
        }

        // 绘制风车支架
        private void drawLines(Canvas canvas) {
            canvas.drawLine(x+width / 3, y+height, x+width / 2, y+ windmillLeng, mPaint);
            canvas.drawLine(x+width / 2, y+ windmillLeng, x+2 * width / 3, y+height, mPaint);
            canvas.drawPath(mPath, mPaint);
        }

        int step=0;
        @Override
        public void move(int maxWidth, int maxHight) {
            step++;
            if(step<delay)
                return;
            step=0;
            if (angle >= 360) angle = 0;
            angle += 5;
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            drawLines(canvas);
            drawWindMills(canvas);
        }
    }
}
